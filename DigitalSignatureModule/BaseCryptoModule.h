#pragma once
#include <string>


class BaseCryptoModule
{
public:
	virtual ~BaseCryptoModule();

	virtual const std::string Sign(const std::string &aMessage) = 0;
	virtual bool Verify(const std::string &aPublicKeyStrHex, const std::string &aMessage, const std::string &aSignatureStrHex) = 0;
};

