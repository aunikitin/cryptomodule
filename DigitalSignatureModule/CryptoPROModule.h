#pragma once
#include "BaseCryptoModule.h"

class CryptoPROModule: public BaseCryptoModule
{
public:
	CryptoPROModule();
	~CryptoPROModule();

	const std::string Sign(const std::string &aMessage) override;
	bool Verify(const std::string &aPublicKeyStrHex, const std::string &aMessage, const std::string &aSignatureStrHex) override;
private:
	void HandleError(char* s);
	void CleanUp();
};

