#pragma once
#include "BaseCryptoModule.h"

#include <string>

struct KeyPairHex {
	std::string PublicKey;
	std::string PrivateKey;
};

class CryptoPPModule: public BaseCryptoModule
{
public:
	CryptoPPModule();
	~CryptoPPModule();

	const std::string Sign(const std::string &aMessage) override;
	bool Verify(const std::string &aPublicKeyStrHex, const std::string &aMessage, const std::string &aSignatureStrHex) override;
private:
	KeyPairHex _pair;
	int _keySize;

	static KeyPairHex RsaGenerateHexKeyPair(unsigned int aKeySize);
};