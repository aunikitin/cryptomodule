#include "CryptoModuleFactory.h"
#include "CryptoPPModule.h"
#include "CryptoPROModule.h"


CryptoModuleFactory::CryptoModuleFactory() = default;
CryptoModuleFactory::~CryptoModuleFactory() = default;

BaseCryptoModule* CryptoModuleFactory::CreateCryptoModule(ModuleType type)
{
	switch (type)
	{
	case ModuleType::CryptoPPType:
		return new CryptoPPModule();
	case ModuleType::CryptoPROType:
		return new CryptoPROModule();
	default: 
		throw std::exception("Impossible to determine module type.");
	}
}

BaseCryptoModule* CreateCryptoModule(ModuleType type)
{
	return CryptoModuleFactory::CreateCryptoModule(type);
}
