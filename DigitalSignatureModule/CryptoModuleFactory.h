#pragma once
#include "BaseCryptoModule.h"

#ifdef DIGITALSIGNATUREMODULE_EXPORTS
#define DLL_API __declspec(dllexport)
#else
#define DLL_API __declspec(dllimport)
#endif

enum ModuleType
{
	CryptoPPType = 0,
	CryptoPROType = 1
};

class CryptoModuleFactory
{
public:
	CryptoModuleFactory();
	~CryptoModuleFactory();

	static BaseCryptoModule* CreateCryptoModule(ModuleType type);
};

extern "C" DLL_API BaseCryptoModule* CreateCryptoModule(ModuleType type);