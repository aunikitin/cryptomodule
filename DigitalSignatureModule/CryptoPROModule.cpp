#include <iostream>
#include "stdafx.h"
#include "stdio.h"
#include "WinCryptEx.h"

#ifdef _WIN32
#   include <windows.h>
#   include <wincrypt.h>
#   include <tchar.h>
#else
#   include <string.h>
#   include <stdlib.h>
#   include <CSP_WinDef.h>
#   include <CSP_WinCrypt.h>
#   include "reader/tchar.h"
#endif

#include "CryptoPROModule.h"

CryptoPROModule::CryptoPROModule()
{
}


CryptoPROModule::~CryptoPROModule()
{
}

//--------------------------------------------------------------------
// В этом примере реализованы создание подписи объекта функции хеширования
// и проверка этой электронно-цифровой подписи.
// Замечание: под win32 рекомендуется использовать _s аналоги CRT функций.
//--------------------------------------------------------------------

#define MY_ENCODING_TYPE  (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
#ifdef _WIN32
#define CONTAINER _TEXT("951cd5323-1e0f-aa95-12c5-51abdf518bd")
#else
#define CONTAINER _TEXT("\\\\.\\HDIMAGE\\test")
#endif

static HCRYPTPROV hProv = 0;
static HCRYPTHASH hHash = 0;
static HCRYPTKEY hKey = 0;
static HCRYPTKEY hPubKey = 0;
static BYTE *pbHash = NULL;
static BYTE *pbSignature = NULL;
static BYTE *pbKeyBlob = NULL;

// TODO: Съехала кодировка. Гит использует UTF-8, с VS17 пришла Windows-1251
// TODO: https://cpdn.cryptopro.ru/content/csp39/html/group___hash_example_SigningHash.html поправить комменты
const std::string CryptoPROModule::Sign(const std::string &aMessage)
{
	//-------------------------------------------------------------
    // Объявление и инициализация переменных.

	BYTE *pbBuffer = (BYTE *)"The data that is to be hashed and signed.";
	DWORD dwBufferLen = (DWORD)(strlen((char *)pbBuffer) + 1);
	DWORD dwSigLen;
	DWORD dwBlobLen;
	DWORD cbHash;

	// Получение дескриптора контекста криптографического провайдера.
	if (CryptAcquireContext(
		&hProv,
		CONTAINER,
		NULL,
		PROV_GOST_2001_DH,
		0))
	{
		printf("CSP context acquired.\n");
	}
	else
	{
		HandleError((char *)"Error during CryptAcquireContext.");
	}
	//--------------------------------------------------------------------
    // Получение открытого ключа подписи. Этот открытый ключ будет 
    // использоваться получателем хеша для проверки подписи.
    // В случае, когда получатель имеет доступ к открытому ключю
    // отправителя с помощью сертификата, этот шаг не нужен.

	if (CryptGetUserKey(
		hProv,
		AT_SIGNATURE,
		&hKey))
	{
		printf("The signature key has been acquired. \n");
	}
	else
	{
		printf("No signature key is available.\n");
		if (GetLastError() == NTE_NO_KEY)
		{
			//-------------------------------------------------------
			// The error was that there is a container but no key.

			// Create a signature key pair. 
			_tprintf(TEXT("The signature key does not exist.\n"));
			_tprintf(TEXT("Create a signature key pair.\n"));
			if (CryptGenKey(
				hProv,
				AT_SIGNATURE,
				0,
				&hKey))
			{
				_tprintf(TEXT("Created a signature key pair.\n"));
			}
			else
			{
				HandleError((char*)"Error occurred creating a signature key.\n");
			}
		}
		else
		{
			HandleError((char*)"An error other than NTE_NO_KEY getting a signature key.\n");
		}
	}
	//--------------------------------------------------------------------
    // Экпорт открытого ключа. Здесь открытый ключ экспортируется в 
    // PUBLICKEYBOLB для того, чтобы получатель подписанного хеша мог 
    // проверить подпись. Этот BLOB может быть записан в файл и передан
    // другому пользователю.

	if (CryptExportKey(
		hKey,
		0,
		PUBLICKEYBLOB,
		0,
		NULL,
		&dwBlobLen))
	{
		printf("Size of the BLOB for the public key determined. \n");
	}
	else
	{
		HandleError((char *)"Error computing BLOB length.");
	}
	//--------------------------------------------------------------------
    // Распределение памяти под pbKeyBlob.

	pbKeyBlob = (BYTE*)malloc(dwBlobLen);
	if (!pbKeyBlob)
		HandleError((char *)"Out of memory. \n");

	// ��� ������� � �������� BLOB.
	if (CryptExportKey(
		hKey,
		0,
		PUBLICKEYBLOB,
		0,
		pbKeyBlob,
		&dwBlobLen))
	{
		printf("Contents have been written to the BLOB. \n");
	}
	else
	{
		HandleError((char *)"Error during CryptExportKey.");
	}
	//--------------------------------------------------------------------
	// �������� ������� ������� �����������.

	if (CryptCreateHash(
		hProv,
		CALG_GR3411,
		0,
		0,
		&hHash))
	{
		printf("Hash object created. \n");
	}
	else
	{
		HandleError((char *)"Error during CryptCreateHash.");
	}

	//--------------------------------------------------------------------
	// �������� ��������� HP_OID ������� ������� �����������.
	//--------------------------------------------------------------------

	//--------------------------------------------------------------------
	// ����������� ������� BLOB� � ������������� ������.

	if (CryptGetHashParam(hHash,
		HP_OID,
		NULL,
		&cbHash,
		0))
	{
		printf("Size of the BLOB determined. \n");
	}
	else
	{
		HandleError((char *)"Error computing BLOB length.");
	}

	pbHash = (BYTE*)malloc(cbHash);
	if (!pbHash)
		HandleError((char *)"Out of memory. \n");

	// ����������� ��������� HP_OID � pbHash.
	if (CryptGetHashParam(hHash,
		HP_OID,
		pbHash,
		&cbHash,
		0))
	{
		printf("Parameters have been written to the pbHash. \n");
	}
	else
	{
		HandleError((char *)"Error during CryptGetHashParam.");
	}

	//--------------------------------------------------------------------
	// ���������� ������������������ ���� ������.

	if (CryptHashData(
		hHash,
		pbBuffer,
		dwBufferLen,
		0))
	{
		printf("The data buffer has been hashed.\n");
	}
	else
	{
		HandleError((char *)"Error during CryptHashData.");
	}

	// ����������� ������� ������� � ������������� ������.
	dwSigLen = 0;
	if (CryptSignHash(
		hHash,
		AT_SIGNATURE,
		NULL,
		0,
		NULL,
		&dwSigLen))
	{
		printf("Signature length %d found.\n", dwSigLen);
	}
	else
	{
		HandleError((char *)"Error during CryptSignHash.");
	}
	//--------------------------------------------------------------------
	// ������������� ������ ��� ����� �������.

	pbSignature = (BYTE *)malloc(dwSigLen);
	if (!pbSignature)
		HandleError((char *)"Out of memory.");

	// ������� ������� ������� �����������.
	if (CryptSignHash(
		hHash,
		AT_SIGNATURE,
		NULL,
		0,
		pbSignature,
		&dwSigLen))
	{
		printf("pbSignature is the hash signature.\n");
	}
	else
	{
		HandleError((char *)"Error during CryptSignHash.");
	}

	// ����������� ������� ������� �����������.
	if (hHash)
		CryptDestroyHash(hHash);

	printf("The hash object has been destroyed.\n");
	printf("The signing phase of this program is completed.\n\n");

	//--------------------------------------------------------------------
	// �� ������ ����� ��������� ����������� �������.
	// ���� ����� �������� �������������� � ������, ����� ��������� 
	// ������������ ���������� ���� � �� �� ���������. ���, �������, 
	// � ����� PUBLICKEYBLOB ����� ���� ��������� �� �����, e-mail ��������� 
	// ��� �� ������� ���������.

	// ����� ����������� ������������ ����� pbBuffer, pbSignature, 
	// szDescription, pbKeyBlob � �� �����.

	// ���������� ������ pbBuffer ������������ �� ���� ��������� 
	// ����������� ����� ������.

	// ��������� szDescription �� �����, ����������� ������, �������������. 
	// ��� ��� �� ����� ����� ��������, ������� ��� ����� �������
	// ������� CryptSignHash.

	//--------------------------------------------------------------------
	// ��������� �������� ����� ������������, ������� ������ �������� �������, 
	// � �������������� ��� � CSP � ������� ������� CryptImportKey. ��� 
	// ���������� ���������� ��������� ����� � hPubKey.

	if (CryptImportKey(
		hProv,
		pbKeyBlob,
		dwBlobLen,
		0,
		0,
		&hPubKey))
	{
		printf("The key has been imported.\n");
	}
	else
	{
		HandleError((char *)"Public key import failed.");
	}
	//--------------------------------------------------------------------
	// �������� ������ ������� ������� �����������.

	if (CryptCreateHash(
		hProv,
		CALG_GR3411,
		0,
		0, 
		&hHash))
	{
		printf("The hash object has been recreated. \n");
	}
	else
	{
		HandleError((char *)"Error during CryptCreateHash.");
	}

	//--------------------------------------------------------------------
	// ���������� ������������������ ���� ������.

	if (CryptHashData(
		hHash,
		pbBuffer,
		dwBufferLen,
		0))
	{
		printf("The new has been created.\n");
	}
	else
	{
		HandleError((char *)"Error during CryptHashData.");
	}

	std::string result = std::string((char *)pbSignature);

	CleanUp();

	return result;
}

bool CryptoPROModule::Verify(const std::string& aPublicKeyStrHex, const std::string& aMessage,
	const std::string& aSignatureStrHex)
{
	//--------------------------------------------------------------------
	// �������� �������� �������.

	DWORD signatureLength = aSignatureStrHex.length();
	return CryptVerifySignature(
		hHash,
		pbSignature,
		signatureLength,
		hPubKey,
		NULL,
		0);
}

void CryptoPROModule::CleanUp(void)
{
	if (pbSignature)
		free(pbSignature);
	if (pbHash)
		free(pbHash);
	if (pbKeyBlob)
		free(pbKeyBlob);

	// ����������� ������� ������� �����������.
	if (hHash)
		CryptDestroyHash(hHash);

	if (hKey)
		CryptDestroyKey(hKey);

	if (hPubKey)
		CryptDestroyKey(hPubKey);

	// ������������ ����������� ����������.
	if (hProv)
		CryptReleaseContext(hProv, 0);
}

//--------------------------------------------------------------------
//  � ���� ������� ������������ ������� HandleError, ������� ���������
//  ������� ������, ��� ������ ��������� � ������ �� ���������. 
//  � ����������� ���������� ��� ������� ���������� ������ ��������, 
//  ������� ������� ����� ������ ��������� �� ������.

void CryptoPROModule::HandleError(char *s)
{
	DWORD err = GetLastError();
	printf("Error number     : 0x%x\n", err);
	printf("Error description: %s\n", s);
	CleanUp();
}