#include "BaseKey.h"
#include <iostream>
#include "./cryptopp/md5.h"
#include "./cryptopp/rng.h"
#include "./cryptopp/hex.h"

BaseKey::BaseKey(char * key, int keySize)
{
	Key = std::string(key);
	KeySize = keySize;
	GetHashFromKey();
}

BaseKey::~BaseKey()
= default;

const char* BaseKey::GetKey() const
{
	return _md5Key.c_str();
}

void BaseKey::ShowInitialKey() const
{
	std::cout << "Hex initial key: ";
	for (auto i = 1; i < KeySize + 1; i++) {
		printf("%02x ", Key[i - 1]);
	}
	std::cout << std::endl;
}

void BaseKey::ShowMd5Key() const
{
	std::cout << "MD5 key: " << _md5Key << std::endl;
}

void BaseKey::ReadKeyWithHashCalculate()
{
	ReadKey();
	GetHashFromKey();
}

int BaseKey::Length() const
{
	return _md5Key.size();
}

void BaseKey::GetHashFromKey()
{
	CryptoPP::MD5 hash;
	CryptoPP::byte digest[CryptoPP::MD5::DIGESTSIZE];

	if (Salt.empty())
	{
		Key = Key + Salt;
	}

	hash.CalculateDigest(digest, (CryptoPP::byte*)Key.c_str(), KeySize);

	CryptoPP::HexEncoder encoder;
	encoder.Attach(new CryptoPP::StringSink(_md5Key));
	encoder.Put(digest, sizeof(digest));
	encoder.MessageEnd();
}
