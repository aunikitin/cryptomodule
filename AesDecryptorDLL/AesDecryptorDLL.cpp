// AesDecryptorDLL.cpp : Defines the exported functions for the DLL application.
//

#include "AesDecryptorDLL.h"

#include "KeyFactory.h"
#include "DialogManager.h"
#include "Decryptor.h"

#include "CryptoModuleFactory.h"
#include "BaseCryptoModule.h"

typedef BaseCryptoModule*(*cryptoProc)(ModuleType type);


bool RunInConsoleMode(char* message, char* signature, long* signatureSize)
{
	BaseKey* key = nullptr;
	do
	{
		delete key;
		const KeyType keyType = DialogManager::AskForKeyType();
		key = KeyFactory::GetKey(keyType);
	} while (!DialogManager::TryFillKey(key));

	Decryptor* decryptor = nullptr;
	size_t libSize = 0;
	do
	{
		delete decryptor;
		std::string fullPath = DialogManager::AskForFilePath();
		decryptor = new Decryptor(key, fullPath);
	} while (!decryptor->TryDecrypt(&libSize) && libSize == 0);

	const LibraryLoader* loader = new LibraryLoader(decryptor->GetLibAsByteArray(), libSize);
	const HMEMORYMODULE handle = loader->LoadLibraryM();

	if (handle == nullptr)
	{
		std::cout << "Can't load library." << std::endl;

		delete loader;
		delete key;
		delete decryptor;

		*signatureSize = 0;
		signature = nullptr;

		return false;
	}
	else
	{
		std::string messageStr = std::string(message);
		std::cout << "Message:" << std::endl;
		std::cout << messageStr << "\n" << std::endl;

		const cryptoProc cryptoModuleCreateFunc = reinterpret_cast<cryptoProc>(LibraryLoader::GetProcAddressM(handle, "CreateCryptoModule"));
		BaseCryptoModule* cryptoModule = cryptoModuleCreateFunc(ModuleType::CryptoPROType);
		const std::string localSign = cryptoModule->Sign(messageStr);

		std::cout << "Signature:" << std::endl;
		std::cout << localSign << "\n" << std::endl;

		delete cryptoModule;
		delete loader;
		delete key;
		delete decryptor;

		LibraryLoader::FreeLibraryM(handle);

		*signatureSize = localSign.length();

		if (signature != nullptr)
		{
			signature = ((char *)localSign.c_str());
		}

		return true;
	}
}

bool Verify(char* message, long messageSize, char* signature)
{
	BaseKey* key = nullptr;
	do
	{
		delete key;
		const KeyType keyType = DialogManager::AskForKeyType();
		key = KeyFactory::GetKey(keyType);
	} while (!DialogManager::TryFillKey(key));

	Decryptor* decryptor = nullptr;
	size_t libSize = 0;
	do
	{
		delete decryptor;
		std::string fullPath = DialogManager::AskForFilePath();
		decryptor = new Decryptor(key, fullPath);
	} while (!decryptor->TryDecrypt(&libSize) && libSize == 0);

	const LibraryLoader* loader = new LibraryLoader(decryptor->GetLibAsByteArray(), libSize);
	const HMEMORYMODULE handle = loader->LoadLibraryM();

	if (handle == nullptr)
	{
		std::cout << "Can't load library." << std::endl;

		delete loader;
		delete key;
		delete decryptor;

		return false;
	}
	else
	{
		std::string messageStr = std::string(message);
		std::cout << "Message:" << std::endl;
		std::cout << messageStr << "\n" << std::endl;

		const cryptoProc cryptoModuleCreateFunc = reinterpret_cast<cryptoProc>(LibraryLoader::GetProcAddressM(handle, "CreateCryptoModule"));
		BaseCryptoModule* cryptoModule = cryptoModuleCreateFunc(ModuleType::CryptoPROType);

		bool result = cryptoModule->Verify("", messageStr, signature);

		delete cryptoModule;
		delete loader;
		delete key;
		delete decryptor;

		LibraryLoader::FreeLibraryM(handle);

		return result;
	}
}