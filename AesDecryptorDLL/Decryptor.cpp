#include "Decryptor.h"
#include <iostream>
#include "../AesEncryptor/cryptopp/hex.h"
#include "../AesEncryptor/cryptopp/default.h"
#include "../AesEncryptor/cryptopp/files.h"

Decryptor::Decryptor(BaseKey* key, const std::string& fullPath)
{
	_key = key;
	_fullPath = std::string(fullPath);
}

bool Decryptor::TryDecrypt(size_t* libSize)
{
	bool successful = true;
	try
	{
		std::cout << "Decryption..." << std::endl;

		_decryptedLib = std::string(Decrypt());
		_libSize = _decryptedLib.size();
		*libSize = _libSize;

		std::cout << "Decryption successful." << std::endl;
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception: " << e.what() << std::endl;
		successful = false;
	}
	return successful;
}

const char* Decryptor::GetLibAsByteArray() const
{
	return _decryptedLib.c_str();
}

std::string Decryptor::Decrypt() const
{
	std::string sourcePath(_fullPath);
	std::string decryptedContent;

	CryptoPP::FileSource fs(sourcePath.c_str(), true,
		new CryptoPP::HexDecoder(
			new CryptoPP::DefaultDecryptorWithMAC(
			(CryptoPP::byte*)_key->GetKey(), _key->Length(),
				new CryptoPP::StringSink(decryptedContent)
			)
		)
	);

	return decryptedContent;
}
