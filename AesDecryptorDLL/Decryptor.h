#pragma once
#include "stdafx.h"
#include <string>

#include "BaseKey.h"
#include "LibraryLoader.h"

class Decryptor final
{
public:
	Decryptor(BaseKey* key, const std::string& fullPath);
	~Decryptor() = default;

	bool TryDecrypt(size_t* libSize);
	const char* GetLibAsByteArray() const;
private:
	std::string _fullPath;
	std::string _decryptedLib;

	size_t _libSize = 0;

	BaseKey* _key = nullptr;

	std::string Decrypt() const;
};

