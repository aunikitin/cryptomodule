#include "BaseService.h"
#include <iostream>

std::string BaseService::ReadDataFromConsole(const std::string& textForOutput)
{
	std::cout << textForOutput << std::endl;

	std::string data;
	std::getline(std::cin, data);

	return data;
}