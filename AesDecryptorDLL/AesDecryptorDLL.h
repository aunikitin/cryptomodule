#pragma once
#include "stdafx.h"

#ifdef AESDECRYPTORDLL_EXPORTS
#define DLL_API __declspec(dllexport)
#else
#define DLL_API __declspec(dllimport)
#endif

extern "C" DLL_API bool RunInConsoleMode(char* message, char* signature, long* signatureSize);
extern "C" DLL_API bool Verify(char* message, long messageSize, char* signature);