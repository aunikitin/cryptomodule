#include "KeyFactory.h"
#include "PassPhraseKey.h"
#include "UsbKey.h"


KeyFactory::KeyFactory()
= default;


KeyFactory::~KeyFactory()
= default;

BaseKey* KeyFactory::GetKey(const KeyType type)
{
	switch (type)
	{
		case Usb:
			return new UsbKey();
		case PassPhrase:
		default:
			return new PassPhraseKey();
	}
}
