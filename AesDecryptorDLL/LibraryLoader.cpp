#include <cstring>
#include <ostream>
#include <iostream>

#include "LibraryLoader.h"
#include "MemoryModule.h"

LibraryLoader::LibraryLoader(const char* libArray, size_t libSize)
{
	_libArray = new char[libSize];
	memcpy(_libArray, libArray, libSize * sizeof(char));
	_libSize = libSize;
}


LibraryLoader::~LibraryLoader()
{
	delete[] _libArray;
}

HMEMORYMODULE LibraryLoader::LoadLibraryM() const
{
	HMEMORYMODULE handle = nullptr;
	try
	{
		handle = MemoryLoadLibrary(_libArray, _libSize);
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception: " << e.what() << std::endl;
	}
	return handle;
}

FARPROC LibraryLoader::GetProcAddressM(const HMEMORYMODULE handle, const LPCSTR functionName)
{
	return MemoryGetProcAddress(handle, functionName);
}

void LibraryLoader::FreeLibraryM(HMEMORYMODULE handle)
{
	MemoryFreeLibrary(handle);
}
