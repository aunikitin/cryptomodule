#pragma once
#include "stdafx.h"
#include <windows.h>

#include "BaseKey.h"

class UsbKey final :
	public BaseKey
{
public:
	UsbKey() = default;
	~UsbKey() = default;

	void ReadKey() override;
private:
	static std::wstring GetDiskIdentificator();
	static std::string GetDriveSerialNumber(const std::wstring& wDrivePath);
};

