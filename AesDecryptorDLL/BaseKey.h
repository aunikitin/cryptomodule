#pragma once
#include "stdafx.h"
#include <string>
#include "./cryptopp/filters.h"

class BaseKey
{
public:
	BaseKey() = default;
	BaseKey(char* key, int keySize);
	virtual ~BaseKey();

	const char* GetKey() const;
	void ShowInitialKey() const;
	void ShowMd5Key() const;
	// Предоставляет базовую реализацию для чтения ключа с последующим вычислением хэша
	void ReadKeyWithHashCalculate();
	int Length() const;
protected:
	std::string Key;
	std::string Salt;

	int KeySize = 0;

	virtual void ReadKey() = 0;
private:
	std::string _md5Key;

	void GetHashFromKey();
};

