#include <windows.h>

#include "UsbKey.h"
#include "BaseService.h"

void UsbKey::ReadKey()
{
	const std::wstring wDrivePath = GetDiskIdentificator();

	Key = std::string(GetDriveSerialNumber(wDrivePath));
	KeySize = sizeof(unsigned long);

	const std::string saltOutputText = "Enter PIN-code below:";
	Salt = std::string(BaseService::ReadDataFromConsole(saltOutputText));
}

std::wstring UsbKey::GetDiskIdentificator()
{
	const std::string textForOutput = "Enter usb device disk identifier as single character:";
	const std::string driveID = BaseService::ReadDataFromConsole(textForOutput);

	std::string drivaPath = R"(\\.\)" + driveID + ":";
	std::wstring wDrivePath = std::wstring(drivaPath.begin(), drivaPath.end());

	return wDrivePath;
}

std::string UsbKey::GetDriveSerialNumber(const std::wstring& wDrivePath)
{
	HANDLE handle = CreateFileW(
		wDrivePath.c_str(),
		GENERIC_READ,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		//FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN,
		//FILE_ATTRIBUTE_NORMAL | FILE_FLAG_RANDOM_ACCESS,
		FILE_ATTRIBUTE_NORMAL,
		NULL
	);

	if (handle == INVALID_HANDLE_VALUE) throw "Error while getting usb device descriptor.";

	static STORAGE_PROPERTY_QUERY spq = { StorageDeviceProperty, PropertyStandardQuery };

	union {
		PVOID buf;
		PSTR psz;
		PSTORAGE_DEVICE_DESCRIPTOR psdd;
	};

	ULONG size = sizeof(STORAGE_DEVICE_DESCRIPTOR) + 0x100;
	ULONG dwError;
	std::string serialNumber;

	do
	{
		dwError = ERROR_NO_SYSTEM_RESOURCES;

		if (buf = LocalAlloc(0, size))
		{
			ULONG BytesReturned;

			if (DeviceIoControl(handle, IOCTL_STORAGE_QUERY_PROPERTY, &spq, sizeof(spq), buf, size, &BytesReturned, 0))
			{
				if (psdd->Version >= sizeof(STORAGE_DEVICE_DESCRIPTOR))
				{
					if (psdd->Size > size)
					{
						size = psdd->Size;
						dwError = ERROR_MORE_DATA;
					}
					else
					{
						if (psdd->SerialNumberOffset)
						{
							PSTR serialNumberP = psz + psdd->SerialNumberOffset;
							serialNumber = std::string(serialNumberP);
							dwError = NOERROR;
						}
						else
						{
							dwError = ERROR_NO_DATA;
						}
					}
				}
				else
				{
					dwError = ERROR_GEN_FAILURE;
				}
			}
			else
			{
				dwError = GetLastError();
			}

			LocalFree(buf);
		}
	} while (dwError == ERROR_MORE_DATA);

	CloseHandle(handle);

	return serialNumber;
}
