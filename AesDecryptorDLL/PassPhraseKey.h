#pragma once
#include "stdafx.h"

#include "BaseKey.h"

class PassPhraseKey final:
	public BaseKey
{
public:
	PassPhraseKey() = default;
	~PassPhraseKey() = default;

	void ReadKey() override;
};

