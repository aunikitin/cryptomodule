#pragma once
#include "stdafx.h"

#include "KeyFactory.h"
#include "BaseKey.h"

class DialogManager final
{
public:
	static bool TryFillKey(BaseKey* key);
	static KeyType AskForKeyType();
	static std::string AskForFilePath();
private:
	DialogManager() = default;
	~DialogManager() = default;
};
