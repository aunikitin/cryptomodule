#pragma once
#include "stdafx.h"

#include "MemoryModule.h"

class LibraryLoader final
{
public:
	LibraryLoader(const char* libArray, size_t libSize);
	~LibraryLoader();

	HMEMORYMODULE LoadLibraryM() const;
	static FARPROC GetProcAddressM(HMEMORYMODULE handle, LPCSTR functionName);
	static void FreeLibraryM(HMEMORYMODULE handle);
private:
	void* _libArray;
	size_t _libSize;
};

