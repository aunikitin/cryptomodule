#include "pch.h"
#include <string>

#include "KeyFactory.h"
#include "DialogManager.h"
#include "Encryptor.h"

int main()
{
	BaseKey* key = nullptr;
	do
	{
		delete key;
		const KeyType keyType = DialogManager::AskForKeyType();
		key = KeyFactory::GetKey(keyType);
	}
	while (!DialogManager::TryFillKey(key));

	Encryptor* encryptor = nullptr;
	do
	{
		delete encryptor;
		std::string fullPath = DialogManager::AskForFilePath();
		encryptor = new Encryptor(key, fullPath);
	}
	while (!encryptor->TryEncrypt());
	
	delete key;
	delete encryptor;

	std::cin;
}
