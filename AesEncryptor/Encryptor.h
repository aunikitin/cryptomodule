#pragma once
#include "BaseKey.h"
#include "cryptopp/base64.h"

class Encryptor
{
public:
	Encryptor(BaseKey* key, const std::string& fullPath);
	~Encryptor() = default;

	bool TryEncrypt() const;
private:
	std::string _fullPath;
	BaseKey* _key;
	std::string Encrypt() const;
	void CopyEncryptedFile() const;
	static void OverwriteFileWithEncryptedContent(const std::string& fullPath, const std::string& content);
};

