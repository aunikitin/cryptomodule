#include "pch.h"
#include "PassPhraseKey.h"
#include "BaseService.h"

void PassPhraseKey::ReadKey()
{
	const std::string textForOutput = "Enter passphrase below:";
	Key = std::string(BaseService::ReadDataFromConsole(textForOutput));
	KeySize = Key.length();
}
