#pragma once
#include <string>

class BaseService final
{
public:
	static std::string ReadDataFromConsole(const std::string& textForOutput);
private:
	BaseService() = default;
	~BaseService() = default;
};

