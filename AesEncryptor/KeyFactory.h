#pragma once
#include "BaseKey.h"

enum KeyType
{
	Usb = 0,
	PassPhrase = 1
};

class KeyFactory final
{
public:
	KeyFactory();
	~KeyFactory();

	static BaseKey* GetKey(KeyType type);
};

