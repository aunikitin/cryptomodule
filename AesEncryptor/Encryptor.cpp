#include "pch.h"
#include "Encryptor.h"
#include "cryptopp/hex.h"
#include "cryptopp/default.h"
#include "cryptopp/files.h"
#include "BaseService.h"

Encryptor::Encryptor(BaseKey* key, const std::string& fullPath)
{
	_key = key;
	_fullPath = std::string(fullPath);
}

bool Encryptor::TryEncrypt() const
{
	bool successful = true;
	try
	{
		std::cout << "Encryption..." << std::endl;

		Encrypt();

		std::cout << "Encryption successful." << std::endl;
	}
	catch(const std::exception& e)
	{
		std::cout << "Exception: " << e.what() << std::endl;
		successful = false;
	}
	return successful;
}

std::string Encryptor::Encrypt() const
{
	CopyEncryptedFile();

	std::string destinationPath(_fullPath);
	destinationPath += ".encrypted";
	std::string encryptedContent;

	CryptoPP::FileSource fs(destinationPath.c_str(), true,
		new CryptoPP::DefaultEncryptorWithMAC(
		(CryptoPP::byte*)_key->GetKey(), _key->Length(),
			new CryptoPP::HexEncoder(
				new CryptoPP::StringSink(encryptedContent)
			)
		)
	);

	OverwriteFileWithEncryptedContent(destinationPath, encryptedContent);

	return encryptedContent;
}

void Encryptor::CopyEncryptedFile() const
{
	std::string destinationPath(_fullPath);
	destinationPath += ".encrypted";
	std::ifstream  src(_fullPath, std::ios::binary);
	std::ofstream  dst(destinationPath.c_str(), std::ios::binary);

	dst << src.rdbuf();
}

void Encryptor::OverwriteFileWithEncryptedContent(const std::string& fullPath, const std::string& content)
{
	std::ofstream encryptedFile(fullPath, std::ios_base::in);
	encryptedFile << content;
}
