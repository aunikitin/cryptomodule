#include "pch.h"
#include <ostream>
#include <iostream>

#include "DialogManager.h"
#include "KeyFactory.h"
#include "BaseService.h"

bool DialogManager::TryFillKey(BaseKey* key)
{
	bool successful = true;
	try
	{
		if (key == nullptr)
		{
			std::cout << "This key type doesn't support." << std::endl;
			throw "This key type doesn't support.";
		}

		key->ReadKeyWithHashCalculate();
		key->ShowInitialKey();
		key->ShowMd5Key();
	}
	catch(const std::exception& e)
	{
		std::cout << "Exception: " << e.what() << std::endl;
		std::cout << "<<----------------------->>" << std::endl;
		successful = false;
	}

	return successful;
}

KeyType DialogManager::AskForKeyType()
{
	using namespace std;
	cout << "0. Auto read key from usb device," << endl;
	cout << "1. Enter passphrase." << endl;

	string sType;
	getline(cin, sType);
	KeyType type;
	try
	{
		type = static_cast<KeyType>(stoi(sType));
	}
	catch (const std::exception& e)
	{
		cout << "Exception: " << e.what() << endl;
		cout << "<<----------------------->>" << endl;
		type = AskForKeyType();
	}

	return type;
}

std::string DialogManager::AskForFilePath()
{
	return BaseService::ReadDataFromConsole("Enter full path to the encrypted file below:");
}
