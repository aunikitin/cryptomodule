#include "pch.h"
#include <iostream>
#include <windows.h>

#include "CryptoModuleFactory.h"
#include "BaseCryptoModule.h"

typedef BaseCryptoModule*(*cryptoProc)(ModuleType type);
typedef bool*(*RunInConsoleModeProc)(char* message, char* signature, long* signatureSize);
typedef bool*(*VerifyProc)(char* message, long messageSize, char* signature);

int main()
{
	HMODULE handle = LoadLibraryW(L"AesDecryptorDLL.dll");
	if (handle == nullptr)
	{
		std::cout << "Can't load library." << std::endl;
	}
	else
	{
		FARPROC proc = GetProcAddress(handle, "RunInConsoleMode");
		const RunInConsoleModeProc runInConsoleMode = reinterpret_cast<RunInConsoleModeProc>(proc);
		long signatureSize = 0;
		long* ptrSignatureSize = &signatureSize;

		char* signature = new char[256];
		char* message = (char*)"secretMessage";
		if (runInConsoleMode((char*)"secretMessage", signature, ptrSignatureSize))
		{
			std::cout << "success" << std::endl;

			//char* trueSign = new char[*ptrSignatureSize];
			//memcpy(trueSign, signature, *ptrSignatureSize);

			// TODO: Verify � CryptoPROModule ��������� � ���������. ������ ��� ������������ ���������� ����������, ��� �� ������������� ���������� �������� ���� ��������������.
			// TODO: ����� ��� ���������� ������ ���������� �� ���������, ����� ���������
			//const VerifyProc verifyProc = reinterpret_cast<VerifyProc>(GetProcAddress(handle, "Verify"));
			//verifyProc(message, 13, trueSign);

			delete[] signature;
			//delete[] trueSign;
		}
	}

	FreeLibrary(handle);
}
